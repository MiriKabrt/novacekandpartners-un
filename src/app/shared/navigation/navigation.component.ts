import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  public isCollapsed = true;

  constructor(
    public translate: TranslateService,
  ) { }

  ngOnInit() {
  }


  changeLangTo(lang) {
    this.translate.use(lang);
  }

}
