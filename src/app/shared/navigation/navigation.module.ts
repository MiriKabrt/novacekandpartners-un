import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NavigationComponent } from './navigation.component';
import { CollapseModule, BsDropdownModule } from 'ngx-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { LocalizeRouterModule } from 'localize-router';

@NgModule({
  imports: [
    BrowserModule,
    CollapseModule,
    BsDropdownModule,
    TranslateModule,
    LocalizeRouterModule,
    ScrollToModule,
  ],
  exports: [
    NavigationComponent,
  ],
  declarations: [
    NavigationComponent
  ]
})
export class NavigationModule { }
