import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { LocalizeRouterModule } from 'localize-router';

@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    LocalizeRouterModule,
  ],
  exports: [
    ModalComponent
  ],
  declarations: [
    ModalComponent
  ]
})
export class AppModalModule { }
