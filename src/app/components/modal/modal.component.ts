import { Component, EventEmitter, Output, TemplateRef, ViewChild, AfterContentInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements AfterContentInit {

  @ViewChild('template') template: TemplateRef<any>;

  @Output() modalClose: EventEmitter<any> = new EventEmitter<any>();

  modalRef: BsModalRef;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private router: Router,
    private modalService: BsModalService) {

  }

  public ngAfterContentInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.openModal();
     }
  }

  public openModal() {
    this.modalService.onHide.subscribe((reason: string) => {
      this.closeModal();
    });
    this.modalRef = this.modalService.show(this.template, {class: 'modal-lg'});
  }

  closeModal() {
    this.router.navigate(['/']);
  }

}
