import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GDPRComponent } from './gdpr.component';
import { AppModalModule } from '../modal/modal.module';
import { LocalizeRouterModule } from 'localize-router';

@NgModule({
  imports: [
    CommonModule,
    AppModalModule,
    LocalizeRouterModule,
  ],
  exports: [
    GDPRComponent
  ],
  declarations: [
    GDPRComponent
  ]
})
export class GDPRModule { }
