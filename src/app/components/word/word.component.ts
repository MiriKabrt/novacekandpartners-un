import { Component, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';


@Component({
  selector: 'app-word',
  templateUrl: './word.component.html',
  styleUrls: ['./word.component.scss'],
  animations: [
    trigger('fade', [
      state('init', style({ opacity: 0 })),
      state('inactive', style({ opacity: 0 })),
      state('active', style({ opacity: 0.7 })),
      transition('init => *', [
        animate('2s 0s')
      ]),
      transition('inactive => active', [
        animate('2s 5s')
      ]),
      transition('active => inactive', [
        animate('2s 1s')
      ])
    ]),
  ]
})
export class WordComponent {

  times = 100;
  counter = 0;
  state = 'init';
  @Input() delay = 1000;

  constructor(
  ) { }

  onDone($event) {
    // call this function at the end of the previous animation.
    // run it as many time as defined
    // tslint:disable-next-line:prefer-const
    let refresh;
    if (this.state === 'init') {
      this.setDelay(refresh);
    } else {
      this.changeState();
    }


  }

  setDelay(refresh) {
    if (refresh) {
      clearTimeout(refresh);
    }
    refresh = setTimeout(() => {
      this.changeState();
    }, this.delay);
  }

  changeState() {
    if (this.counter < this.times) {
      this.state = this.state === 'active' ? 'inactive' : 'active';
      this.counter++;
    }
  }
}
