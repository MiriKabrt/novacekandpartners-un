import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WordComponent } from './word.component';
import { LocalizeRouterModule } from 'localize-router';

@NgModule({
  imports: [
    CommonModule,
    LocalizeRouterModule
  ],
  exports: [
    WordComponent
  ],
  declarations: [
    WordComponent
  ]
})
export class WordModule { }
