import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { AppModalModule } from '../modal/modal.module';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from 'localize-router';

@NgModule({
  imports: [
    CommonModule,
    AppModalModule,
    TranslateModule,
    LocalizeRouterModule,
  ],
  exports: [
    UserComponent
  ],
  declarations: [
    UserComponent
  ]
})
export class UserModule { }
