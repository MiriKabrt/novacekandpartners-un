import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  public modal = {
    name: '',
    title: '',
    desc: '',
    info: null
  };

  value: '';

  public info = {
    'novacek': {
      'img': 'novacek',
      'linkedIn': 'https://linkedin.com/in/novacekjan/',
      'email': 'jan.novacek@novacekandpartners.com',
      'phone': '+420 739 685 356'
    },
    'sanders': {
      'img': 'sanders',
      'linkedIn': 'https://www.linkedin.com/in/alena-sanders-5049b636/',
      'email': 'alena.sanders@novacekandpartners.com',
      'phone': '+420 606 096 006'
    },
    'kabrt': {
      'img': 'kabrt',
      'linkedIn': 'https://www.linkedin.com/in/tkabrt/',
      'email': 'tomas.kabrt@novacekandpartners.com',
      'phone': '+420 731 245 232'
    },
    'tisnovska': {
      'img': 'tisnovska',
      'linkedIn': 'https://www.linkedin.com/in/eva-tisnovska-41b6a236/',
      'email': 'eva.tisnovska@novacekandpartners.com',
      'phone': '+420 603 720 840'
    },
    'valouchova': {
      'img': 'valouchova',
      'linkedIn': 'https://www.linkedin.com/in/annavalouchova/',
      'email': 'anna.valouchova@novacekandpartners.com'
    },
    'rezkova': {
      'img': 'rezkova',
      'linkedIn': 'https://www.linkedin.com/in/lucie-rezkov%C3%A1-30282a47/',
      'email': 'lucie.rezkova@novacekandpartners.com',
      'phone': '+420 733 161 540'
    },
    'neureiterova': {
      'img': 'neureiterova',
    },
    'brizova': {
      'img': 'brizova',
    },
  };


  constructor(
    private translate: TranslateService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.value = params.value; // --> Name must match wanted parameter
      this.getInfo(this.value);
    });
  }

  async getInfo(name) {
    const tran = await this.translate.get(['head.' + name + '.name', 'head.' + name + '.title', 'head.' + name + '.desc']).toPromise();
    this.modal.name = tran['head.' + name + '.name'];
    this.modal.title = tran['head.' + name + '.title'];
    this.modal.desc = tran['head.' + name + '.desc'];
    this.modal.info = this.info[name];
  }
}
