import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceClientsComponent } from './service-clients.component';
import { AppModalModule } from '../modal/modal.module';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from 'localize-router';

@NgModule({
  imports: [
    CommonModule,
    AppModalModule,
    TranslateModule,
    LocalizeRouterModule,
  ],
  exports: [
    ServiceClientsComponent
  ],
  declarations: [
    ServiceClientsComponent
  ]
})
export class ServiceClientsModule { }
