import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceCandidateComponent } from './service-candidate.component';
import { AppModalModule } from '../modal/modal.module';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from 'localize-router';

@NgModule({
  imports: [
    CommonModule,
    AppModalModule,
    TranslateModule,
    LocalizeRouterModule,
  ],
  exports: [
    ServiceCandidateComponent
  ],
  declarations: [
    ServiceCandidateComponent
  ]
})
export class ServiceCandidateModule { }
