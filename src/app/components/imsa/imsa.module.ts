import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IMSAComponent } from './imsa.component';
import { AppModalModule } from '../modal/modal.module';
import { LocalizeRouterModule } from 'localize-router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    AppModalModule,
    TranslateModule,
    LocalizeRouterModule,
  ],
  exports: [
    IMSAComponent
  ],
  declarations: [
    IMSAComponent
  ]
})
export class IMSAModule { }
