import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-imsa',
  templateUrl: './imsa.component.html',
  styleUrls: ['./imsa.component.scss']
})
export class IMSAComponent {

  constructor(
    public translate: TranslateService,
  ) {

  }

}
