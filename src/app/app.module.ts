import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Location } from '@angular/common';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NavigationModule } from './shared/navigation/navigation.module';
import { CollapseModule, BsDropdownModule, CarouselModule, ModalModule } from 'ngx-bootstrap';
import { HomeModule } from './sections/home/home.module';
import { ReferenceModule } from './sections/reference/reference.module';
import { HeadhuntingModule } from './sections/headhunting/headhunting.module';
import { ServicesModule } from './sections/services/services.module';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ContactModule } from './sections/contact/contact.module';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { GDPRModule } from './components/gdpr/gdpr.module';
import { GDPRComponent } from './components/gdpr/gdpr.component';
import { AppModalModule } from './components/modal/modal.module';
import { ServiceClientsModule } from './components/service-clients/service-clients.module';
import { ServiceClientsComponent } from './components/service-clients/service-clients.component';
import { ServiceCandidateModule } from './components/service-candidate/service-candidate.module';
import { ServiceCandidateComponent } from './components/service-candidate/service-candidate.component';
import { UserModule } from './components/user/user.module';
import { UserComponent } from './components/user/user.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WordModule } from './components/word/word.module';
import { LocalizeRouterModule, LocalizeRouterSettings, LocalizeParser, ManualParserLoader } from 'localize-router';
import { LocalizeRouterHttpLoader } from 'localize-router-http-loader';
import { Observable } from 'rxjs';
import { IMSAComponent } from './components/imsa/imsa.component';
import { IMSAModule } from './components/imsa/imsa.module';



// export class TranslateUniversalLoader implements TranslateLoader {
//   public getTranslation(lang: string): Observable<any> {
//     return Observable.create(observer => {
//       observer.next(JSON.parse(readFileSync(`/assets/locales/${lang}.json`, 'utf8')));
//       observer.complete();
//     });
//   }
// }

// export class LocalizeUniversalLoader extends LocalizeParser {

//   public load(routes: Routes): Promise<any> {
//     return new Promise((resolve: any) => {
//       const data: any = require(`/assets/locales/routes.json`);
//       this.locales = data.locales;
//       this.prefix = data.prefix;
//       this.init(routes).then(resolve);
//     });
//   }
// }

// export function localizeLoaderFactory(translate: TranslateService, location: Location) {
//   return new LocalizeUniversalLoader(translate, location, new LocalizeRouterSettings());
// }


const appRoutes: Routes = [

  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  }, {
    path: 'gdpr',
    component: GDPRComponent
  }, {
    path: 'imsa',
    component: IMSAComponent
  }, {
    path: 'service-clients',
    component: ServiceClientsComponent,
  }, {
    path: 'service-candidate',
    component: ServiceCandidateComponent,
  }, {
    path: 'headhunter/:value',
    component: UserComponent,
  }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    RouterModule.forRoot(appRoutes),
    // LocalizeRouterModule.forRoot(appRoutes, {
    //   parser: {
    //     provide: LocalizeParser,
    //     useFactory: (translate, location, settings) =>
    //       new ManualParserLoader(translate, location, settings, ['cs', 'en'], 'YOUR_PREFIX'),
    //     deps: [TranslateService, Location, LocalizeRouterSettings]
    //   }
    // }),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    ScrollToModule.forRoot(),
    NavigationModule,
    HomeModule,
    HeadhuntingModule,
    ReferenceModule,
    ServicesModule,
    ContactModule,
    GDPRModule,
    IMSAModule,
    ServiceClientsModule,
    ServiceCandidateModule,
    UserModule,
    AppModalModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

// export function localizeRouterParserFactory(translate, location, settings, http) {
//   return new LocalizeRouterHttpLoader(translate, location, settings, http, './assets/i18n/routes.json');
// }



