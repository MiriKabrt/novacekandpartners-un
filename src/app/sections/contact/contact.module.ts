import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact.component';
import { LocalizeRouterModule } from 'localize-router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    LocalizeRouterModule,
  ],
  exports: [
    ContactComponent,
  ],
  declarations: [
    ContactComponent
  ]
})
export class ContactModule { }
