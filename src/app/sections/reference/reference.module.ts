import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReferenceComponent } from './reference.component';
import { CarouselModule } from 'ngx-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from 'localize-router';

@NgModule({
  imports: [
    BrowserModule,
    TranslateModule,
    CarouselModule,
    LocalizeRouterModule,
  ],
  exports: [
    ReferenceComponent,
  ],
  declarations: [
    ReferenceComponent
  ]
})
export class ReferenceModule { }
