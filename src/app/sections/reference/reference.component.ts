import { Component, OnInit } from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap';

@Component({
  selector: 'app-reference',
  templateUrl: './reference.component.html',
  styleUrls: ['./reference.component.scss'],
  providers: [
    { provide: CarouselConfig, useValue: { interval: 50000, noPause: true, showIndicators: true } }
  ]
})
export class ReferenceComponent implements OnInit {

  public items = ['ref1', 'ref2', 'ref3', 'ref4', 'ref5', 'ref6', 'ref7', 'ref8', 'ref9', 'ref10', 'ref11', 'ref12'];

  constructor() { }

  ngOnInit() {
  }

}
