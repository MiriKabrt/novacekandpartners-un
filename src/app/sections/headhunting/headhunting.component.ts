import { Component, OnInit, TemplateRef, AfterViewInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';


@Component({
  selector: 'app-headhunting',
  templateUrl: './headhunting.component.html',
  styleUrls: ['./headhunting.component.scss'],
})
export class HeadhuntingComponent implements OnInit, AfterViewInit {

  public scrollbarOptions = { axis: 'x', theme: 'dark-thick' };

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private translate: TranslateService,
    private mScrollbarService: MalihuScrollbarService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {

    if (isPlatformBrowser(this.platformId)) {
   this.mScrollbarService.initScrollbar('#malihu-scrollbar', { axis: 'x', theme: 'dark-thick' });
    }
  }


}
