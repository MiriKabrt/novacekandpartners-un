import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeadhuntingComponent } from './headhunting.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { WordModule } from '../../components/word/word.module';
import { LocalizeRouterModule } from 'localize-router';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule,
    WordModule,
    LocalizeRouterModule,
    NgScrollbarModule,
    MalihuScrollbarModule.forRoot(),
  ],
  exports: [
    HeadhuntingComponent
  ],
  declarations: [
    HeadhuntingComponent
  ]
})
export class HeadhuntingModule { }
