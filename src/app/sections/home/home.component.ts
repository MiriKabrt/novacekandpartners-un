import { Component, OnInit, HostBinding } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @HostBinding('style.width') width: Number;
  @HostBinding('style.height') height: Number;

  constructor(
    public translate: TranslateService,
  ) { }

  ngOnInit() {
    this.height = this.width;
  }

}
